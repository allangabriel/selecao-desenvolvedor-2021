## Prova de SQL
A prova de SQL por simplicidade será feita em SQLite, seu banco de dados se encontra neste repositório, sendo ele
o arquivo `backend/chinook.db`, e caso você não possua um cliente SQLite, você pode encontrar um no seguinte link:
https://portableapps.com/apps/development/sqlite_database_browser_portable

Você pode encontrar um diagrama sobre o que tem disponível neste banco na imagem `chinook_db_schema.jpg`.

## Questões
1 - Utilizando o banco de dados fornecido no arquivo chinook.db prepare uma consulta que mostre o nome do artista e a quantidade albuns que ele lançou, ordenados por quem mais tem albuns.
    Colunas da resposta:
        ArtistName | QtdeAlbums
    ```sql
    SELECT artists.Name AS "ArtistName",
       COUNT(albums.AlbumId) AS "QtdeAlbums"
    FROM artists
    JOIN albums ON artists.ArtistId = albums.ArtistId
    GROUP BY artists.ArtistId ORDER BY "QtdeAlbums" DESC;
    ```

2 - Prepare uma consulta que traga os 20 clientes que mais gastaram. (Na tabela invoices há as compras feitas por cada cliente e seu valor)
    Colunas da resposta:
        CustomerFullName | Total
    ```sql
    SELECT customers.FirstName || " " || customers.LastName AS "CustomerFullName",
           SUM(invoices.Total) AS "Total"
    FROM customers
    JOIN invoices ON customers.CustomerId = invoices.CustomerId
    GROUP BY customers.CustomerId
    ORDER BY 2 DESC
    LIMIT 20;
    ```

3 - Listar gênero musical com o valor vendido, e a quantidade de vendas.
    Colunas da resposta:
        Genre | TotalSold | QtdeSold
    ```sql
    SELECT genres.name AS "Genre" ,
	    SUM(invoice_items.UnitPrice * invoice_items.Quantity) AS "TotalSold",
        SUM(invoice_items.Quantity) AS "QtdeSold"
    FROM genres 
    JOIN tracks ON genres.GenreId = tracks.GenreId 
    JOIN invoice_items ON tracks.TrackId = invoice_items.TrackId
    GROUP BY 1;
    ```
 
4 - Listar os albuns com preço, duração total em minutos, tamanho em MB.
    Colunas da resposta:
        AlbumTitle | Price | Duration_minutes | Size_MB
    ```sql
    SELECT albums.Title AS "AlbumTitle", 
	    SUM(tracks.UnitPrice) AS "Price",
        SUM(tracks.Milliseconds / 1000 / 60) AS "Duration_minutes",
        SUM(tracks.Bytes*0.000001) AS "Size_MB"
    FROM albums 
    JOIN tracks ON albums.AlbumId = tracks.AlbumId 
    WHERE tracks.UnitPrice IS NOT NULL
    GROUP BY 1;
    ```

5 - Listar empregados com números de clientes, quanto cada um vendeu até o momento, gênero musical que mais vende em qtd (mais popular), e em valor (mais lucrativo).
    Colunas da resposta:
        EmployeeId | EmployeeFullName | QtdeCustomers | TotalSold | MostPopularGenre | MostLucrativeGenre

6 - Consulta que traga a lista de gêneros musicais com 12 colunas (Janeiro a Dezembro) com a qtd de faixas vendidas de um determinado ano a ser especificado num filtro.
    Colunas da resposta:
        GenreId | GenreName | Jan | Feb | Mar | Apr | May | Jun | Jul | Aug | Sep | Oct | Nov | Dec
    ```sql 
    SELECT genres.GenreId,
        genres.Name,
        SUM(CASE
                WHEN strftime("%m", invoices.InvoiceDate) = "01" THEN invoice_items.Quantity
                ELSE 0
            END) AS "Jan",
        SUM(CASE
                WHEN strftime("%m", invoices.InvoiceDate) = "02" THEN invoice_items.Quantity
                ELSE 0
            END) AS "Feb",
        SUM(CASE
                WHEN strftime("%m", invoices.InvoiceDate) = "03" THEN invoice_items.Quantity
                ELSE 0
                END) AS "Mar",
        SUM(CASE
                WHEN strftime("%m", invoices.InvoiceDate) = "04" THEN invoice_items.Quantity
                ELSE 0
            END) AS "Apr",
        SUM(CASE
                WHEN strftime("%m", invoices.InvoiceDate) = "05" THEN invoice_items.Quantity
                ELSE 0
            END) AS "May",
        SUM(CASE
                WHEN strftime("%m", invoices.InvoiceDate) = "06" THEN invoice_items.Quantity
                ELSE 0
            END) AS "Jun",
            
        SUM(CASE
                WHEN strftime("%m", invoices.InvoiceDate) = "07" THEN invoice_items.Quantity
                ELSE 0
            END) AS "Jul",
            
        SUM(CASE
                WHEN strftime("%m", invoices.InvoiceDate) = "08" THEN invoice_items.Quantity
                ELSE 0
            END) AS "Aug",
            
        SUM(CASE
                WHEN strftime("%m", invoices.InvoiceDate) = "09" THEN invoice_items.Quantity
                ELSE 0
            END) AS "Sep",
        SUM(CASE
                WHEN strftime("%m", invoices.InvoiceDate) = "10" THEN invoice_items.Quantity
                ELSE 0
            END) AS "Oct",
            SUM(CASE
                WHEN strftime("%m", invoices.InvoiceDate) = "11" THEN invoice_items.Quantity
                ELSE 0
            END) AS "Nov",
        SUM(CASE
                WHEN strftime("%m", invoices.InvoiceDate) = "12" THEN invoice_items.Quantity
                ELSE 0
            END) AS "Dec"
    FROM genres
    JOIN tracks ON genres.GenreId = tracks.GenreId
    JOIN invoice_items ON tracks.TrackId = invoice_items.TrackId
    JOIN invoices ON invoice_items.InvoiceId = invoices.InvoiceId
    WHERE strftime("%Y", invoices.InvoiceDate) = "2009"
    GROUP BY 1;
    ```

7 - Listar supervisores (aqueles funcionários a quem os outros se reportam)
    Há um funcionário que não se reporta a ninguém, este não precisará vir na listagem.
    Há diversos funcionários que ninguém se reporta a eles, este também não devem vir na listagem.
    Aos funcionários que virão na listagem, deverá ser exibida o nome deles, e 12 colunas de meses (Jan-Dez) com o valor que foi vendido por este, ou por alguma das pessoas que se reportam a ele. E outras 12 colunas de meses com a quantidade de faixas vendidas por ele, ou alguém que se reporte a ele.
    Esta tabela será utilizada para gerar gráficos de rendimento das equipes de cada supervisor.
    Deverá conter também uma coluna listando quantos clientes aquela equipe possui.
    Colunas da resposta:
        EmployeeId | EmployeeFullName | QtdeCustomers | 
        TotalSold_Jan | TotalSold_Feb | TotalSold_Mar | TotalSold_Apr | TotalSold_May | TotalSold_Jun |
        TotalSold_Jul | TotalSold_Aug | TotalSold_Sep | TotalSold_Oct | TotalSold_Nov | TotalSold_Dec |
        QtdeTracksSold_Jan | QtdeTracksSold_Feb | QtdeTracksSold_Mar | QtdeTracksSold_Apr | QtdeTracksSold_May | QtdeTracksSold_Jun |
        QtdeTracksSold_Jul | QtdeTracksSold_Aug | QtdeTracksSold_Sep | QtdeTracksSold_Oct | QtdeTracksSold_Nov | QtdeTracksSold_Dec

8 - Criar uma View que possibilite mostrar os dados da lista de supervisores mencionada acima (questão 7), e que possibilite ser filtrada por ano.
    Quero fazer a consulta simplesmente com `select * from vw_lista_supervisores where ano = 2015`.
    Atenção: A resolução dessa questão é a apresentação do script de criação dessa View. E não a criação dela dentro do banco de
        dados que se encontra neste repositório. Se o candidato apenas criar a view dentro do banco de dados mas não apresentar
        o script por escrito na prova, será considerado como não tendo respondido.
