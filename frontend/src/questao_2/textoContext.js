import { createContext, useState } from "react";

const TextoContext = createContext();

const TextoProvider = ({ children }) => {
  const [texto, setTexto] = useState("Texto");

  return (
    <TextoContext.Provider value={{ texto, setTexto }}>
      {children}
    </TextoContext.Provider>
  );
};

const withTexto = (Child) => (props) => (
  <TextoContext.Consumer>
    {(context) => <Child {...props} {...context} />}
  </TextoContext.Consumer>
);

export { TextoProvider, withTexto };
