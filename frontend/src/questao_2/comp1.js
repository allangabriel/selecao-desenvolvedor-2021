import { withTexto } from "./textoContext";

function Comp1({ texto }) {
  return <div>{texto}</div>;
}

export default withTexto(Comp1);
