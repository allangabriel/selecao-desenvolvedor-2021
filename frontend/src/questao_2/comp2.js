import { withTexto } from "./textoContext";

function Comp2({ texto, setTexto }) {
  return (
    <div>
      <input value={texto} onChange={(event) => setTexto(event.target.value)} />
    </div>
  );
}

export default withTexto(Comp2);
