import Comp1 from "./comp1";
import Comp2 from "./comp2";
import { TextoProvider } from "./textoContext";

function Questao2() {
  return (
    <div>
      <h1>Questão 2</h1>
      <TextoProvider>
        <Comp1 />
        <Comp2 />
      </TextoProvider>
    </div>
  );
}

export default Questao2;
