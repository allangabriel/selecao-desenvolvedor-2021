async function chamarPromise(x) {
  if (x > 7) {
    return "Deu certo";
  } else {
    throw new Error("Deu errado");
  }
}

function acaoBotao() {
  chamarPromise(8)
    .then((res) => alert(res))
    .catch((err) => alert(err));
}

async function acaoBotao2() {
  try {
    const res = await chamarPromise(8);
    alert(res);
  } catch (err) {
    alert(err);
  }
}

function Questao4() {
  return (
    <div>
      <h1>Questão 4</h1>
      <button onClick={acaoBotao}>Ativar</button>
      <button onClick={acaoBotao2}>Ativar2</button>
    </div>
  );
}

export default Questao4;
