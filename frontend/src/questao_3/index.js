import { useState, useEffect } from "react";

function Questao3() {
  const [valor, setValor] = useState("");

  useEffect(() => {
    const parsedValor = localStorage.getItem("valor") || 0;
    setValor(parsedValor);
  }, []);

  useEffect(() => {
    localStorage.setItem("valor", valor);
  }, [valor]);

  return (
    <div>
      <h1>Questão 3</h1>
      <input value={valor} onChange={(event) => setValor(event.target.value)} />
    </div>
  );
}

export default Questao3;
